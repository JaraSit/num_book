# Úvod

Jedná se o sérii Jupyter-notebooků zaměřených na aplikovanou numerickou matematiku, implementovanou zejména v programovacím jazyce Python.

:::{note}
Jedná se o výukovou pomůcku, která neprošla žádnou revizí ani jinou kontrolou. V textu lze tedy nalézt nedostatky nebo nekorektní výsledky. V případě takového nálezu, prosím o zpětnou vazbu.
:::

:::{tableofcontents}
:::
